# moved to codeberg

<https://codeberg.org/jonas-l/private-commit>

## private-commit

When you use git and make a commit, then the timestamp of the commit is saved.
However, you eventually work at public git repositories and don't want a public protocol of your working times.
This is the solution.

This small JavaScript application rounds the current date
down to the start of the last week/ hour/ day, sets ``GIT_AUTHOR_DATE`` and ``GIT_COMMITTER_DATE`` accordingly and runs git commit.

To install it from the registry, run ``npm i -g private-commit``.
To install it if you checked it out from git, use ``npm link``.

To use it, install it globally (so that you can use it anytime) and use ``p[x]commit`` instead of ``git commit`` to make commits. Command line parameters are passed through, so you can use
``p[x]commit -a`` and it will execute ``git commit -a``.

Note: Replace the x
- by ``ẁ`` for rounding to the previous monday
- by ``d`` for rounding to the day
- by ``h`` for rounding to the hour

Warning: There is still some logging due to the logged push events, e.g. at <https://gitlab.com/l-jonas/private-commit/activity>.
